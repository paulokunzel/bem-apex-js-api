# BEM-APEX JS API

Uma bilioteca complementar a biblioteca [APEX.JS](https://docs.oracle.com/en/database/oracle/application-express/21.2/aexjs/index.html)

---

# Indice
## Como Utilizar
## Tópicos
## Ultimas Atualizações
## Atualizações Anteriores

---

# Como utilizar
Dentro do seu projeto Oracle APEX, adicionar o arquivo antes no global ou nas paǵinas que ele será utilizado.

O mapeamento imita o do objeto apex, sendo que bastaria alterar para bem o prefixo

Exemplo: 
- apex.submit -> bem.submit
- apex.message.showErrors -> bem.message.showErrors

Está biblioteca tem por objetivo siomplificar algumas das funções disponiveis no objeto apex e alterar alguns comportamentos para evitar as mesmas verificações copiadas e coladas por diversos sistemas.

Exemplos: 
- Função bem.server.process e bem.submit: Verfica se o app está online antes de executar
- bem.submit: exibe por default o "Show Loading" para evitar que o usuário consiga clicar ou executar multiplos processos ao mesmo tempo

# Tópicos
## Log
Módulo responsavel pelas funcionalidades de log de erros em uma tabela definida pelo desenvolvedor.

## Message
Módulo de mensangens paralelo ao apex.message na funções compartilhadas entre ambos

## Page
Módulo de funcionalidades da página paralelo ao apex.page na funções compartilhadas entre ambos

## Server
Módulo de comunicação com o servidor paralelo ao apex.server na funções compartilhadas entre ambos

## Utils
Módulo de funções utilitarias paralelo ao apex.server na funções compartilhadas entre ambos


# Ultimas Atualizações
- Criação de função de validação de JSON
- Atualização do modulo de log para ser configuravel
- Função de formatar para valores monetários


# Atualizações anteriores
- Criação do módulo 'log'
	- Gravar log no banco
- Criação do módulo 'message'
	- Exibibr mensagem de erro
- Criação do módulo 'page'
	- Efetuar submit da página
- Criação do módulo 'server'
	- Criados objetos padrões
	- Criada função de chamada ajax
- Criação do módulo 'utils'
	- Criada função de isonline
	- Criada função para verifica se objeto está vazio

