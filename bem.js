const bem = {
	/**
	 * @description Inicializa o as configurações padrões.
	 * @param {Object} config Objeto com configurações adicionais
	 */
	initialize: (config) => {
		const configuration = {
			...config,
			successMessageTimeOut: 3000
		};

		bem.message.setSuccessMessageTimeOut(configuration.successMessageTimeOut);
	},

	/**
	 * @description Módulo responsável pelo log de dados no banco.
	 */
	log: {
		/**
		 * @description Função de atalho que inicializa as variaveis necessárias para ativar o log
		 * @param {Object} config Objeto de configuração do log
		 */
		initialize: (config) => {
			bem.log.saveLog = config.saveLog;
			bem.log.logEnabled(config.logEnabled)
		},

		/**
		 * @description Configura o objeto de log
		 * @param {Boolean | Object} logStatus 
		 */
		logEnabled: (logStatus) => {
			if (typeof logStatus == 'boolean') {
				for (const property in bem.log.logType) bem.log.logType[property].enabled = logStatus;
			} else {
				bem.log.logType = Object.assign(bem.log.logType, logStatus);
			}
		},

		/**
		 * @description Mensagens padrões de log no banco
		 */
		logType: {
			script: {
				enabled: false,
				description: 'Erro de Script'
			},
			callbackStatus: {
				enabled: false,
				description: 'Falhar de validação no servidor'
			},
			callbackProcess: {
				enabled: false,
				description: 'Falha ao receber dados do servidor'
			},
			requiredFields: {
				enabled: false,
				description: 'Campos obrigatorios não recebidos'
			}
			/* 
			TODO: Implementar funcionalidade de log quando offiline. guardar informação dentro variavel?
			connection: {
				enabled : false,
				description: 'Queda na conexão'
			}*/
		},

		/**
		 * @description Função que executa o log. Precisa ser definida antes de utilizar
		 * @param {Object} dataSent Os dados que serão enviados
		 */
		saveLog: (dataSent) => { throw 'Função de Log não configurada' }
	},

	/**
	 * @description Módulo com funções de formatação para regiões
	 */
	locale: {
		/**
		 * @description Converte valor fornecido em formatação monetária
		 * @param {number} value O valor a ser convertido
		 * @param {number} numDecimals o número de casas decimais. Padrão = 2
		 * @returns Uma string formatada
		 */
		formatToCurrency: (value, numDecimals = 2) => apex.locale.formatNumber(value, `FML999G999G999G999G999D${'0'.repeat(numDecimals)}`)
	},

	/**
	 * @description Módulo que gerencia as mensagens exibidas em tela.
	 */
	message: {
		/**
		 * @description Remove a mensagem de sucesso após X milisegundos
		 * @param {Integer} time Tempo em milisegundos maior que zero.
		 */
		setSuccessMessageTimeOut: (time) => {
			if (time && time > 0) {
				apex.message.setThemeHooks({
					beforeShow: function (pMsgType, pElement$) {
						if (pMsgType == apex.message.TYPE.SUCCESS) {
							setTimeout(function () { $('.t-Alert').fadeOut('slow');	}, time);
						}
					}
				});
			}
		},
		/**
		 * @description Exibe uma unica mensagem de erro se passar uma string ou multiplas se passar uma lista igual ao padrão do apex.message.showErrors. Limpa mensagem anteriores
		 * @param {String || Array[Object]} message A(s) Mensagem(ns) de erro
		 */
		showErrors: (message) => {
			apex.message.clearErrors();

			// Se message for uma string, converte para a lista padrão necessária para o APEX.
			if (typeof message == 'string') {
				message = ([{
					type: "error",
					location: "page",
					message: message
				}
				]);
			}

			apex.message.showErrors(message);
		}
	},

	/**
	 * @description Módulo de postagem de dados
	 */
	page: {
		/**
		 * @description Efetua o submit da tela. Exibe o "show processing" do APEX.
		 * @param {String | Object} options A String do Request ou o objeto de configuração APEX
		 */
		submit: (options) => {
			if (!bem.util.isOnline) return;

			if (typeof options == 'string' || options == undefined) {
				apex.submit({ requet: options, showWait: true });
			} else {
				apex.submit(Object.assign({ showWait: true }, options));
			}
		}
	},

	/**
	 * @description Módulo de comunicação AJAX
	 */
	server: {
		/**
		 * @description Objeto padrão de erros de callback. Exibe uma mensagem em tela, executa log no console e no banco.
		 * @param {Object} jsonResponse JSON detalhada do erro ocorrido.
		 * @param {String} textStatus Mensagem generica do erro
		 * @param {Object} errorThrown dados adicionais do erro
		 */
		ajaxErrorCallback: (jsonResponse, textStatus, errorThrown = {}) => {
			bem.message.showErrors(textStatus);
			console.error('jsonResponse', jsonResponse, 'textStatus', textStatus, 'errorThrown', errorThrown, 'Process', processName);

			if (bem.log.callbackProcess.enabled) {
				//Atribui todos os inputs a um unico objeto
				const pageItems = Array.from(document.querySelectorAll('input'))
					.filter(elemento => elemento.id)
					.map(elemento => ({ [elemento.id]: elemento.value }))
					.reduce((a, b) => Object.assign(a, b), {})

				const dataSent = {
					x20: textStatus
					, x19: bem.log.logType.callbackProcess
					, x18: JSON.stringify(pageItems)
				};

				bem.log.saveLog(dataSent);
			};
		},

		/**
		 * @description Função que gera um objeto para as chamadas AJAX. Compativel com apex.server.process e jquery.ajax
		 * @param {Object} config Um objeto com configurações adicionais
		 * @returns O objeto gerado.
		 */
		ajaxCallback: (config) => ({ ...config, success: console.log, error: bem.server.ajaxErrorCallback }),

		/**
		 * @description Função que retorna o objeto padrão para executar a abertura de telas modais
		 * @returns O objeto gerado.
		 */
		ajaxCallbackModal: () => bem.server.ajaxCallback({ dataType: 'text', success: eval }),

		/**
		 * @description Função que gera um objeto padrão para chamadas com retorno JSON
		 * @param {String} successCallback o que fazer em se a chamada objetve sucesso.
		 * @returns O objeto gerado.
		 */
		ajaxCallbackStandard: (successCallback) => bem.server.ajaxCallback({ success: successCallback }),

		/**
		 * @description Função equivalente ao apex.server.process, confere se o existe conexão antes de executar.
		 * @param {String} nomeProcesso O nome do processo AJAX
		 * @param {Object} dadosEnviados Objeto que atualiza as variaveis no servidor
		 * @param {Object} configCallback Configuração da execução da callback.
		 */
		process: (nomeProcesso, dadosEnviados = null, configCallback = bem.server.ajaxCallback) => { if (bem.util.isOnline(nomeProcesso)) apex.server.process(nomeProcesso, dadosEnviados, configCallback); },
	},

	/**
	 * @description Efetua o submit da tela (ver bem.page.submit). Exibe o "show processing" do APEX.
	 * @param {String | Object} options A String do Request ou o objeto de configuração APEX
	 */
	submit: (options) => bem.page.submit(options),

	/**
	 * @description Módulo com funões utilitarias
	 */
	util: {
		/**
		 * @description Verifica se o objeto em questão está vazio
		 * @param {Object} object o objeto a ser testado
		 * @returns True se está vazio
		 */
		isEmpty: (object) => Object.keys(object).length === 0 && object.constructor === Object,

		/**
		 * @description Verifica se o sistema está online e grava no log caso não esteja
		 * @param {String} nomeProcesso (opcional) O nome do processo chamado
		 * @returns true se estiver online.
		 */
		isOnline: (nomeProcesso) => {
			if (navigator.onLine) return true;

			//logError('Offline', nomeProcesso, tipoOcorrencia.connexao);
			ajaxErrorMessage(null, 'Sistema Offline - Conecte na internet e tente novamente');
			return false;
		},

		/**
		 * @description Função que válida o json recebido
		 * @param {JSON} jsonReceived O Json a ser validado
		 * @param {JSON} jsonParameters Objeto com os parametros de validação
		 * @returns True se o JSON recebido for válido.
		 */
		jsonValidation: (jsonReceived, jsonParameters) => {
			let erros = '';

			for (const property in jsonParameters) {
				if (jsonParameters[property].required
					&& jsonReceived[property] == null) {
					erros += `Parâmetro ${property} não definido`;
				} else if (jsonReceived[property] != null
					&& typeof jsonReceived[property] != jsonParameters[property].type) {
					erros += `Tipo errado para parâmetro ${property}`;
				}
			}

			if (erros.length > 0) {
				bem.message.showErrors(erros);
				return false;
			}

			return true;
		},

		/**
		 * @description converte os valores de uma lista de elementos HTML em um CSV
		 * @param {String} querySelector o seletor usadopara capturar os elementos.
		 * @param {String} atributo O atributo na tag que contem o valor.
		 * @param {String} separator O tipo de separador.
		 * @returns 
		 */
		convertToCSV: (querySelector, atributo = 'value', separator = ',') => {
			const selector = Array.from(document.querySelectorAll(querySelector));
			const valor = selector.map(item => atributo == 'value' ? item.value : item.getAttribute(atributo));
			const separado = valor.join(separator);

			return separado;
		}
	}
}